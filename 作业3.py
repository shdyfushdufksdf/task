import requests
import pytest
import allure

@allure.title("新增宠物测试")
def test_add_pet():
    url = "https://petstore.swagger.io/v2/pet"
    headers = {"Content-Type": "application/json"}
    data = {
        "id": 1,
        "category": {
            "id": 0,
            "name": "string"
        },
        "name": "doggie",
        "photoUrls": [
            "string"
        ],
        "tags": [
            {
                "id": 0,
                "name": "string"
            }
        ],
        "status": "available"
    }

    response = requests.post(url, json=data, headers=headers)

    assert response.status_code == 200
    assert response.json()["id"] == 1

@allure.title("查询宠物测试")
def test_find_pet_by_status():
    url = "https://petstore.swagger.io/v2/pet/findByStatus"
    params = {"status": "available"}

    response = requests.get(url, params=params)

    assert response.status_code == 200
    assert len(response.json()) > 0