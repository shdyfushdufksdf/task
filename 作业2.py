import allure
import requests
import pytest


class TestPet:
    url = "https://petstore.swagger.io/v2/pet"

    @staticmethod
    @allure.step("创建宠物")
    def create_pet(data):
        try:
            r = requests.post(url=TestPet.url, json=data)
            return r
        except Exception as e:
            print(f"create_pet请求发送失败，异常信息：{e}")
            return None

    @pytest.mark.parametrize("data, expected_status_code", [
        ({"id": 1, "name": "cat", "status": "available"}, 200),
        ({"id": 2, "name": "", "status": "pending"}, 200),
        ({"id": 3, "name": "dog", "status": "sold"}, 400),
    ])
    def test_create_pet(self, data, expected_status_code):
        with allure.step("Step 1: 发送请求"):
            r = self.create_pet(data)
        with allure.step("Step 2: 断言状态码"):
            assert r.status_code == expected_status_code

    @staticmethod
    @allure.step("根据宠物状态查询宠物")
    def find_pet_by_status(status):
        params = {"status": status}
        r = requests.get(url=f"{TestPet.url}/findByStatus", params=params)
        return r

    @pytest.mark.parametrize("status, expected_status_code", [
        ("available", 200),
        ("pending", 200),
        ("sold", 200),
        ("unknown", 400),
    ])
    def test_find_pet_by_status(self, status, expected_status_code):
        with allure.step("Step 1: 发送请求"):
            r = self.find_pet_by_status(status)
        with allure.step("Step 2: 断言状态码"):
            assert r.status_code == expected_status_code